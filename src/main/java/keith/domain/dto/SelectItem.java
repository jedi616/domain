package keith.domain.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Getter
@Setter
public class SelectItem
{
    private long value;
    private String label;
}
