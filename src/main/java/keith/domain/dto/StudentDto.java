package keith.domain.dto;

import java.util.Date;
import java.util.List;

import keith.domain.type.GenderType;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Getter
@Setter
public class StudentDto
{
    private long studentId;
    private String name;
    private GenderType gender;
    private String placeOfBirth;
    private Date birthDate;
    private CourseDto course;
    private List<SelectItem> courseSelectItems;
}
