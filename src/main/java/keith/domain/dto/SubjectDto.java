package keith.domain.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Getter
@Setter
public class SubjectDto
{
    private long subjectId;
    private String subjectName;
}
