package keith.domain.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Getter
@Setter
public class CourseDto
{
    private long courseId;
    private String courseName;
}
