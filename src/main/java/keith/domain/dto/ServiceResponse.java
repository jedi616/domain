package keith.domain.dto;

import static keith.domain.util.KeithUtils.getErrorMessage;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Getter
@Setter
public class ServiceResponse<D> {

    enum Status {
        SUCCESS, ERROR
    }

    private D data;
    private Status status = Status.SUCCESS;
    private String message;

    public boolean isSuccess() {
        return status == Status.SUCCESS;
    }

    public void success(D data, String message) {
        this.data = data;
        this.status = Status.SUCCESS;
        this.message = message;
    }

    public void error(String message) {
        this.status = Status.ERROR;
        this.message = message;
    }

    public void error(Throwable throwable) {
        error(getErrorMessage(throwable));
    }
}
