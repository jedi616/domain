package keith.domain.type;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public enum GenderType
{
    Male, Female
}
