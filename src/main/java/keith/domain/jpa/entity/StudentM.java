package keith.domain.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import keith.domain.type.GenderType;

/**
 * The persistent class for the student_m database table.
 * 
 */
@Entity
@Table(name = "student_m")
@NamedQuery(name = "StudentM.findAll", query = "SELECT s FROM StudentM s")
public class StudentM implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long studentId;

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date")
    private Date birthDate;

    @Enumerated(EnumType.STRING)
    private GenderType gender;

    private String name;

    @Column(name = "place_of_birth")
    private String placeOfBirth;

    // bi-directional many-to-one association to StudEnrollmentT
    @OneToMany(mappedBy = "studentM")
    private List<StudEnrollmentT> studEnrollmentTs;

    // bi-directional many-to-one association to CourseM
    @ManyToOne
    @JoinColumn(name = "course_id")
    private CourseM courseM;

    public StudentM()
    {
    }

    public Long getStudentId()
    {
        return this.studentId;
    }

    public void setStudentId(Long studentId)
    {
        this.studentId = studentId;
    }

    public Date getBirthDate()
    {
        return this.birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public GenderType getGender()
    {
        return this.gender;
    }

    public void setGender(GenderType gender)
    {
        this.gender = gender;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPlaceOfBirth()
    {
        return this.placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth)
    {
        this.placeOfBirth = placeOfBirth;
    }

    public List<StudEnrollmentT> getStudEnrollmentTs()
    {
        return this.studEnrollmentTs;
    }

    public void setStudEnrollmentTs(List<StudEnrollmentT> studEnrollmentTs)
    {
        this.studEnrollmentTs = studEnrollmentTs;
    }

    public StudEnrollmentT addStudEnrollmentT(StudEnrollmentT studEnrollmentT)
    {
        getStudEnrollmentTs().add(studEnrollmentT);
        studEnrollmentT.setStudentM(this);

        return studEnrollmentT;
    }

    public StudEnrollmentT removeStudEnrollmentT(StudEnrollmentT studEnrollmentT)
    {
        getStudEnrollmentTs().remove(studEnrollmentT);
        studEnrollmentT.setStudentM(null);

        return studEnrollmentT;
    }

    public CourseM getCourseM()
    {
        return this.courseM;
    }

    public void setCourseM(CourseM courseM)
    {
        this.courseM = courseM;
    }

}
