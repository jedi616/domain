package keith.domain.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the stud_enrollment_subject_t database table.
 * 
 */
@Entity
@Table(name = "stud_enrollment_subject_t")
@NamedQuery(name = "StudEnrollmentSubjectT.findAll", query = "SELECT s FROM StudEnrollmentSubjectT s")
public class StudEnrollmentSubjectT implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stud_enroll_subj_id")
    private Long studEnrollSubjId;

    @Temporal(TemporalType.DATE)
    @Column(name = "enrollment_date")
    private Date enrollmentDate;

    private String schedule;

    // bi-directional many-to-one association to StudEnrollmentT
    @ManyToOne
    @JoinColumn(name = "std_enroll_id")
    private StudEnrollmentT studEnrollmentT;

    // bi-directional many-to-one association to SubjectM
    @ManyToOne
    @JoinColumn(name = "subject_id")
    private SubjectM subjectM;

    public StudEnrollmentSubjectT()
    {
    }

    public Long getStudEnrollSubjId()
    {
        return this.studEnrollSubjId;
    }

    public void setStudEnrollSubjId(Long studEnrollSubjId)
    {
        this.studEnrollSubjId = studEnrollSubjId;
    }

    public Date getEnrollmentDate()
    {
        return this.enrollmentDate;
    }

    public void setEnrollmentDate(Date enrollmentDate)
    {
        this.enrollmentDate = enrollmentDate;
    }

    public String getSchedule()
    {
        return this.schedule;
    }

    public void setSchedule(String schedule)
    {
        this.schedule = schedule;
    }

    public StudEnrollmentT getStudEnrollmentT()
    {
        return this.studEnrollmentT;
    }

    public void setStudEnrollmentT(StudEnrollmentT studEnrollmentT)
    {
        this.studEnrollmentT = studEnrollmentT;
    }

    public SubjectM getSubjectM()
    {
        return this.subjectM;
    }

    public void setSubjectM(SubjectM subjectM)
    {
        this.subjectM = subjectM;
    }

}
