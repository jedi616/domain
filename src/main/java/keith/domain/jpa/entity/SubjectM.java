package keith.domain.jpa.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the subject_m database table.
 * 
 */
@Entity
@Table(name = "subject_m")
@NamedQuery(name = "SubjectM.findAll", query = "SELECT s FROM SubjectM s")
public class SubjectM implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subject_id")
    private Long subjectId;

    @Column(name = "subject_name")
    private String subjectName;

    // bi-directional many-to-one association to StudEnrollmentSubjectT
    @OneToMany(mappedBy = "subjectM")
    private List<StudEnrollmentSubjectT> studEnrollmentSubjectTs;

    public SubjectM()
    {
    }

    public Long getSubjectId()
    {
        return this.subjectId;
    }

    public void setSubjectId(Long subjectId)
    {
        this.subjectId = subjectId;
    }

    public String getSubjectName()
    {
        return this.subjectName;
    }

    public void setSubjectName(String subjectName)
    {
        this.subjectName = subjectName;
    }

    public List<StudEnrollmentSubjectT> getStudEnrollmentSubjectTs()
    {
        return this.studEnrollmentSubjectTs;
    }

    public void setStudEnrollmentSubjectTs(List<StudEnrollmentSubjectT> studEnrollmentSubjectTs)
    {
        this.studEnrollmentSubjectTs = studEnrollmentSubjectTs;
    }

    public StudEnrollmentSubjectT addStudEnrollmentSubjectT(StudEnrollmentSubjectT studEnrollmentSubjectT)
    {
        getStudEnrollmentSubjectTs().add(studEnrollmentSubjectT);
        studEnrollmentSubjectT.setSubjectM(this);

        return studEnrollmentSubjectT;
    }

    public StudEnrollmentSubjectT removeStudEnrollmentSubjectT(StudEnrollmentSubjectT studEnrollmentSubjectT)
    {
        getStudEnrollmentSubjectTs().remove(studEnrollmentSubjectT);
        studEnrollmentSubjectT.setSubjectM(null);

        return studEnrollmentSubjectT;
    }

}
