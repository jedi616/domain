package keith.domain.jpa.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the course_m database table.
 * 
 */
@Entity
@Table(name = "course_m")
@NamedQuery(name = "CourseM.findAll", query = "SELECT c FROM CourseM c")
public class CourseM implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private Long courseId;

    @Column(name = "course_name")
    private String courseName;

    // bi-directional many-to-one association to StudentM
    @OneToMany(mappedBy = "courseM")
    private List<StudentM> studentMs;

    // bi-directional many-to-one association to DepartmentM
    @ManyToOne
    @JoinColumn(name = "department_id")
    private DepartmentM departmentM;

    public CourseM()
    {
    }

    public Long getCourseId()
    {
        return this.courseId;
    }

    public void setCourseId(Long courseId)
    {
        this.courseId = courseId;
    }

    public String getCourseName()
    {
        return this.courseName;
    }

    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }

    public List<StudentM> getStudentMs()
    {
        return this.studentMs;
    }

    public void setStudentMs(List<StudentM> studentMs)
    {
        this.studentMs = studentMs;
    }

    public StudentM addStudentM(StudentM studentM)
    {
        getStudentMs().add(studentM);
        studentM.setCourseM(this);

        return studentM;
    }

    public StudentM removeStudentM(StudentM studentM)
    {
        getStudentMs().remove(studentM);
        studentM.setCourseM(null);

        return studentM;
    }

    public DepartmentM getDepartmentM()
    {
        return this.departmentM;
    }

    public void setDepartmentM(DepartmentM departmentM)
    {
        this.departmentM = departmentM;
    }

}
