package keith.domain.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the stud_enrollment_t database table.
 * 
 */
@Entity
@Table(name = "stud_enrollment_t")
@NamedQuery(name = "StudEnrollmentT.findAll", query = "SELECT s FROM StudEnrollmentT s")
public class StudEnrollmentT implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "std_enroll_id")
    private Long stdEnrollId;

    @Temporal(TemporalType.DATE)
    @Column(name = "enrollment_date")
    private Date enrollmentDate;

    private String semester;

    // bi-directional many-to-one association to StudEnrollmentSubjectT
    @OneToMany(mappedBy = "studEnrollmentT")
    private List<StudEnrollmentSubjectT> studEnrollmentSubjectTs;

    // bi-directional many-to-one association to StudentM
    @ManyToOne
    @JoinColumn(name = "student_id")
    private StudentM studentM;

    public StudEnrollmentT()
    {
    }

    public Long getStdEnrollId()
    {
        return this.stdEnrollId;
    }

    public void setStdEnrollId(Long stdEnrollId)
    {
        this.stdEnrollId = stdEnrollId;
    }

    public Date getEnrollmentDate()
    {
        return this.enrollmentDate;
    }

    public void setEnrollmentDate(Date enrollmentDate)
    {
        this.enrollmentDate = enrollmentDate;
    }

    public String getSemester()
    {
        return this.semester;
    }

    public void setSemester(String semester)
    {
        this.semester = semester;
    }

    public List<StudEnrollmentSubjectT> getStudEnrollmentSubjectTs()
    {
        return this.studEnrollmentSubjectTs;
    }

    public void setStudEnrollmentSubjectTs(List<StudEnrollmentSubjectT> studEnrollmentSubjectTs)
    {
        this.studEnrollmentSubjectTs = studEnrollmentSubjectTs;
    }

    public StudEnrollmentSubjectT addStudEnrollmentSubjectT(StudEnrollmentSubjectT studEnrollmentSubjectT)
    {
        getStudEnrollmentSubjectTs().add(studEnrollmentSubjectT);
        studEnrollmentSubjectT.setStudEnrollmentT(this);

        return studEnrollmentSubjectT;
    }

    public StudEnrollmentSubjectT removeStudEnrollmentSubjectT(StudEnrollmentSubjectT studEnrollmentSubjectT)
    {
        getStudEnrollmentSubjectTs().remove(studEnrollmentSubjectT);
        studEnrollmentSubjectT.setStudEnrollmentT(null);

        return studEnrollmentSubjectT;
    }

    public StudentM getStudentM()
    {
        return this.studentM;
    }

    public void setStudentM(StudentM studentM)
    {
        this.studentM = studentM;
    }

}
