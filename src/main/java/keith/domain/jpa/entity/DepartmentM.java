package keith.domain.jpa.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the department_m database table.
 * 
 */
@Entity
@Table(name = "department_m")
@NamedQuery(name = "DepartmentM.findAll", query = "SELECT d FROM DepartmentM d")
public class DepartmentM implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "department_id")
    private Long departmentId;

    @Column(name = "department_name")
    private String departmentName;

    // bi-directional many-to-one association to CourseM
    @OneToMany(mappedBy = "departmentM")
    private List<CourseM> courseMs;

    public DepartmentM()
    {
    }

    public Long getDepartmentId()
    {
        return this.departmentId;
    }

    public void setDepartmentId(Long departmentId)
    {
        this.departmentId = departmentId;
    }

    public String getDepartmentName()
    {
        return this.departmentName;
    }

    public void setDepartmentName(String departmentName)
    {
        this.departmentName = departmentName;
    }

    public List<CourseM> getCourseMs()
    {
        return this.courseMs;
    }

    public void setCourseMs(List<CourseM> courseMs)
    {
        this.courseMs = courseMs;
    }

    public CourseM addCourseM(CourseM courseM)
    {
        getCourseMs().add(courseM);
        courseM.setDepartmentM(this);

        return courseM;
    }

    public CourseM removeCourseM(CourseM courseM)
    {
        getCourseMs().remove(courseM);
        courseM.setDepartmentM(null);

        return courseM;
    }

}
