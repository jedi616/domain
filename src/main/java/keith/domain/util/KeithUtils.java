package keith.domain.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import keith.domain.dto.SelectItem;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public class KeithUtils
{
    private static final String UTF_8 = "UTF-8";
    private static final String UNSUPPORTED_CHARACTER = "<UNSUPPORTED_CHARACTER>";

    public static String getErrorMessage(Throwable throwable)
    {
        return (throwable.getMessage() != null) ? throwable.getMessage() : throwable.toString();
    }

    public static String encodeMessage(String message)
    {
        try
        {
            return URLEncoder.encode(message, UTF_8);
        }
        catch (UnsupportedEncodingException e)
        {
            // Returning empty string for Unsupported characters (UnsupportedEncodingException), not throwing the catched Exception as this
            // method is also used within catch statements.
            return UNSUPPORTED_CHARACTER;
        }
    }

    public static void validateSelectedItem(long id, List<SelectItem> selectItems) throws Exception
    {
        boolean found = false;
        for (SelectItem selectItem : selectItems)
        {
            if (id == selectItem.getValue())
            {
                found = true;
                break;
            }
        }
        if (!found)
        {
            throw new Exception("Selected Item with id=" + id + " does not match with SelectItem List");
        }
    }

    public static PropertySourcesPlaceholderConfigurer properties(ApplicationContext applicationContext, Environment environment)
            throws Exception
    {
        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        String myappConfigLocation = environment.getProperty("myapp.config.location");
        String[] paths = myappConfigLocation.split(",");
        Resource[] locations = new Resource[paths.length];
        for (int i = 0; i < paths.length; i++)
        {
            locations[i] = new FileSystemResource(paths[i]);
        }
        pspc.setLocations(locations);
        return pspc;
    }
}
