package keith.domain.type;

import org.junit.Test;

import junit.framework.Assert;

public class GenderTypeTest
{
    @Test
    public void valueOfTest()
    {
        GenderType genderType = GenderType.valueOf("Female");
        Assert.assertEquals(GenderType.Female, genderType);
    }
}
