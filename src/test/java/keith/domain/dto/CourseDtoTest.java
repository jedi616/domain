package keith.domain.dto;

import org.junit.Test;

import junit.framework.Assert;

public class CourseDtoTest {

    @Test
    public void test1() {
	CourseDto courseDto = new CourseDto();
	courseDto.setCourseName("BSCS");
	Assert.assertEquals(courseDto.getCourseName(), "BSCS");
    }
}
